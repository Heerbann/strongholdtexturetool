// Stronghold Texture Tool
// Thanks to: MaxxLu, elfofthefire
// Web:	http://strongholdmods.forumphp3.net
//		http://stronghold.wikia.com
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
)

var (
	op           string
	input        string
	verbose      bool
	build        bool
	buildTileset bool
	set          int
)

func init() {
	flag.StringVar(&input, "i", "./input", "directory or file to convert")
	flag.BoolVar(&verbose, "verbose", false, "show debug info (values: true, false)")
	flag.BoolVar(&build, "build", true, "build collections (values: true, false)")
	flag.BoolVar(&buildTileset, "buildTileSet", true, "build tileset (values: true, false)")
	flag.IntVar(&set, "set", 0, "gm1 set i.e. teamcolor (values: 0 to 9). Colorset 0 for civilians.")
}

func main() {
	fmt.Println("TGX/GM1 Converter")

	flag.Parse()

	stat, err := os.Stat(input)

	if err != nil {
		println(err)
		return
	}

	if stat.IsDir() {
		//convert all files in dir
		info, err := ioutil.ReadDir(input)
		if err != nil {
			log.Fatal("main: Cannot open dir: " + input)
		}

		for _, e := range info {
			if !e.IsDir() {
				filepath := input + "/" + e.Name()
				ext := path.Ext(filepath)
				switch ext {
				case ".tgx":
					var tgx TGXImage
					tgx.Load(filepath)
					tgx.Save()
				case ".gm1":
					var gm1 GM1Image
					gm1.Load(filepath)
					gm1.SmartSave()
				}
			}
		}
	} else {
		//convert a single file
		ext := path.Ext(input)
		switch ext {
		case ".tgx":
			var tgx TGXImage
			tgx.Load(input)
			tgx.Save()
		case ".gm1":
			var gm1 GM1Image
			gm1.Load(input)
			gm1.SmartSave()
		}
	}
}
