# README # dd
A tool to convert tgx/gm1 assets used by Stronghold 1 & Stronghold Crusader to png images. 

### Usage ###
TGX/GM1 Converter

```
Usage of ./strongholdTextureTool:
  -build
        build collections (values: true, false) (default true)
  -buildTileSet
        build tileset (values: true, false) (default true)
  -i string
        directory or file to convert (default "./input")
  -set int
        gm1 set i.e. teamcolor (values: 0 to 9). Colorset 0 for civilians.
  -verbose
        show debug info (values: true, false)
```

Example:
```
./strongholdTextureTool -i="path/to/your/stronghold/gm"
```

### Acknowledgment ###
Thanks to the authors of 

http://stronghold.wikia.com/wiki/TGX_file_format

http://stronghold.wikia.com/wiki/GM1_file_format 

for providing information about the gm1/tgx formats.

### License ###

MIT, see LICENSE