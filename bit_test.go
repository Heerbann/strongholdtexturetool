package main

import "testing"

func TestBitExtract(t *testing.T) {
	red := 1<<0 | 1<<1 | 1<<2
	rede := extractBitsetValue(red, 0, 4)

	if red != rede {
		t.Error("Error: ", red, "!=", rede)
	}

	//bit testing
	testBit := 1 << 5 //set bit 5
	if bitTest(testBit, 6) == true {
		t.Error("Bittest failed")
	}
	if bitTest(testBit, 5) == false {
		t.Error("Bittest failed")
	}
	if bitTest(testBit, 4) == true {
		t.Error("Bittest failed")
	}
}
