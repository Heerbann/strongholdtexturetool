package main

import (
	"encoding/binary"
	"image"
	"image/draw"
	"image/png"
	"log"
	"math"
	"os"
	"path"
	"strconv"
)

/*
GM1 File:
	File Header
	Color Pallet
	Image Offset List
	Image Size List
	Image Header List
	Image Data List
*/

const (
	TYPE_TGX          = iota + 1 //interface items, building animations (no file header!)
	TYPE_ANIMATION               //animations
	TYPE_TGXTILE                 //Stored in tile format
	TYPE_TGXFONT                 //Font
	TYPE_BITMAP                  //Walls, grass, stones and more (no compression, 16bit per pixel)
	TYPE_TGXCONSTSIZE            //All images have the same size
	TYPE_BITMAPOTHER             //not realy used... (no compression)
)

const (
	GM1TILEBYTES  = 512
	GM1TILEWIDTH  = 30
	GM1TILEHEIGHT = 16
)

const (
	DIRECTION_NONE = iota
	DIRECTION_DOWN
	DIRECTION_RIGHT
	DIRECTION_LEFT
)

var GM1TilePixelsPerLine = [...]int{2, 6, 10, 14, 18, 22, 26, 30, 30, 26, 22, 18, 14, 10, 6, 2} //Tiles are 30x16 px (diamond shape)
var PathTypeTable = [...]string{"nopath", "tgx", "animations", "tile", "font", "bitmap", "tgxconst", "bitmapother"}

type GM1Header struct {
	u0         [3]int32
	ImageCount int32
	u1         int32
	DataType   int32
	u2         [2]int32
	SizeType   int32
	u3         [11]int32
	DataSize   int32 //size of the file left to read in bytes
	u4         int32
}

type GM1ImageHeader struct {
	Width            uint16
	Height           uint16
	PositionX        uint16
	PositionY        uint16
	Part             byte   //tile only
	Parts            byte   //tile only
	TilePositionY    uint16 //tile only
	Direction        byte
	HorizontalOffset byte //tile only
	DrawingBoxWdith  byte //tile only
	PerformanceID    byte
}

type GM1EntryInfo struct {
	Offset uint32
	Size   uint32
	Header GM1ImageHeader
	Image  Image
	Tile   Image
}

func (this *GM1Header) Read(reader *os.File) {
	binary.Read(reader, binary.LittleEndian, &this.u0)
	binary.Read(reader, binary.LittleEndian, &this.ImageCount)
	binary.Read(reader, binary.LittleEndian, &this.u1)
	binary.Read(reader, binary.LittleEndian, &this.DataType)
	binary.Read(reader, binary.LittleEndian, &this.u2)
	binary.Read(reader, binary.LittleEndian, &this.SizeType)
	binary.Read(reader, binary.LittleEndian, &this.u3)
	binary.Read(reader, binary.LittleEndian, &this.DataSize)
	binary.Read(reader, binary.LittleEndian, &this.u4)
}

func (this *GM1ImageHeader) Read(reader *os.File) {
	binary.Read(reader, binary.LittleEndian, &this.Width)
	binary.Read(reader, binary.LittleEndian, &this.Height)
	binary.Read(reader, binary.LittleEndian, &this.PositionX)
	binary.Read(reader, binary.LittleEndian, &this.PositionY)
	binary.Read(reader, binary.LittleEndian, &this.Part)
	binary.Read(reader, binary.LittleEndian, &this.Parts)
	binary.Read(reader, binary.LittleEndian, &this.TilePositionY)
	binary.Read(reader, binary.LittleEndian, &this.Direction)
	binary.Read(reader, binary.LittleEndian, &this.HorizontalOffset)
	binary.Read(reader, binary.LittleEndian, &this.DrawingBoxWdith)
	binary.Read(reader, binary.LittleEndian, &this.PerformanceID)
}

type GM1Image struct {
	filename string
	entries  []GM1EntryInfo
	header   GM1Header
	palette  TGXPalette
}

func (this *GM1Image) Load(filename string) {
	this.filename = filename

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal("Load: cannot open file", err)
	}

	//Read header
	this.header.Read(file)

	//Read Palette
	this.palette.Read(file)

	//Read entry data
	this.entries = make([]GM1EntryInfo, this.header.ImageCount)
	//offsets
	for i, _ := range this.entries {
		binary.Read(file, binary.LittleEndian, &this.entries[i].Offset)
	}
	//sizes
	for i, _ := range this.entries {
		binary.Read(file, binary.LittleEndian, &this.entries[i].Size)
	}

	//Read headers
	for i, _ := range this.entries {
		this.entries[i].Header.Read(file)
	}

	endOfHeader, _ := file.Seek(0, 1) //all offsets are relative to this

	//Read images
	log.Println("GM1: Type: ", PathTypeTable[this.header.DataType], " File:", filename)

	for i, entry := range this.entries {
		//seek to position
		file.Seek(int64(entry.Offset)+endOfHeader, 0)

		switch this.header.DataType {
		case TYPE_BITMAP:
			logVerbose("GM1: Bitmap Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeBitmap(entry.Header, endOfHeader+int64(entry.Offset), int64(entry.Size), file)

		case TYPE_TGX, TYPE_TGXFONT, TYPE_TGXCONSTSIZE:
			logVerbose("GM1: TGX Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: entry.Header.Width, Height: entry.Header.Height}, endOfHeader+int64(entry.Offset), int64(entry.Size), file, nil)

		case TYPE_ANIMATION:
			logVerbose("GM1: TGX (animated) Width", entry.Header.Width, " Height: ", entry.Header.Height)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: entry.Header.Width, Height: entry.Header.Height}, endOfHeader+int64(entry.Offset), int64(entry.Size), file, &this.palette)

		case TYPE_TGXTILE:
			logVerbose("GM1: Tile")
			this.entries[i].Tile.DecodeTile(endOfHeader+int64(entry.Offset), file)
			this.entries[i].Image.DecodeTGX(TGXHeader{Width: uint16(entry.Header.DrawingBoxWdith), Height: entry.Header.TilePositionY + GM1TILEHEIGHT}, endOfHeader+int64(entry.Offset)+GM1TILEBYTES, int64(entry.Size)-GM1TILEBYTES, file, nil)

		default:
			log.Println("Unhandled Type")
		}
	}
}

func (this *GM1Image) Save() {
	//create output dir
	_, filename := path.Split(this.filename)
	os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)

	for i, entry := range this.entries {
		savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + strconv.Itoa(int(entry.Header.PerformanceID)) + "_" + strconv.Itoa(int(entry.Header.Parts)) + "img" + strconv.Itoa(i)
		entry.Image.Save(savePath, "png")
		entry.Tile.Save(savePath+"_TILE", "png")
	}
}

func (this *GM1Image) SaveCollection() {

	//create collections
	var collections [][]GM1EntryInfo

	collectionIndex := 0
	i := 0

	for _, entry := range this.entries {
		//new collection
		if entry.Header.Part == 0 {
			logVerbose("New Collection: Parts:", entry.Header.Parts)
			collections = append(collections, make([]GM1EntryInfo, entry.Header.Parts))
			i = 0
		}

		if entry.Header.Parts == 0 {
			continue
		}

		//insert image to collection
		collections[collectionIndex][i] = entry
		i++

		//end of collection
		if entry.Header.Part == entry.Header.Parts-1 {
			logVerbose("End of Collection")
			collectionIndex++
		}
	}

	//build final images i.e. compiling everything into one image
	//mostly for debugging purpose
	for c, collection := range collections {

		//bounding box
		top := math.MaxInt32
		left := math.MaxInt32
		bottom := 0
		right := 0

		//find bounds of the sub-images
		for i := 0; i < len(collection); i++ {
			x := int(collection[i].Header.PositionX)
			y := int(collection[i].Header.PositionY)
			w := int(collection[i].Header.Width)
			h := int(collection[i].Header.Height)

			left = min(left, x)
			top = min(top, y)
			right = max(right, x+w)
			bottom = max(bottom, y+h)
		}

		//calculate the actual dimensions of the image
		width := right - left
		height := bottom - top

		//create image
		img := image.NewRGBA(image.Rect(0, 0, width, height))

		//draw all entities of the collection
		for i := 0; i < len(collection); i++ {
			//draw the tile part
			x := int(collection[i].Header.PositionX) - left
			y := int(collection[i].Header.PositionY+collection[i].Header.TilePositionY) - top
			rect := image.Rect(x, y, x+GM1TILEWIDTH, y+GM1TILEHEIGHT)
			draw.Draw(img, rect, collection[i].Tile.data, image.ZP, draw.Over)

			//draw the image part
			x = int(collection[i].Header.PositionX+uint16(collection[i].Header.HorizontalOffset)) - left
			y = int(collection[i].Header.PositionY) - top
			rect = image.Rect(x, y, x+int(collection[i].Header.DrawingBoxWdith), y+int(collection[i].Header.Height))
			draw.Draw(img, rect, collection[i].Image.data, image.ZP, draw.Over)

			//Note: E.g. Buildings consist of tiles (foundation) and images (walls etc.)
		}

		//save collection to disk
		_, filename := path.Split(this.filename)
		os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)
		savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + "collection" + strconv.Itoa(c)
		file, _ := os.Create(savePath + ".png")
		png.Encode(file, img)
		file.Close()
	}
}

func (this *GM1Image) SaveImageset() {
	numberOfEntries := len(this.entries)

	width := 0
	height := 0
	x := 0

	//find imageset size
	for i := 0; i < numberOfEntries; i++ {
		//ignore tiles
		if len(this.entries[i].Image.data.Pix) == 0 {
			continue
		}

		width += int(this.entries[i].Header.DrawingBoxWdith)
		height = max(height, int(this.entries[i].Header.Height))
	}

	//create image
	img := image.NewRGBA(image.Rect(0, 0, width, height))

	//draw images
	for i := 0; i < numberOfEntries; i++ {
		//ignore tiles
		if len(this.entries[i].Image.data.Pix) == 0 {
			continue
		}

		rect := image.Rect(x, 0, x+int(this.entries[i].Header.DrawingBoxWdith), int(this.entries[i].Header.Height))
		draw.Draw(img, rect, this.entries[i].Image.data, image.ZP, draw.Over)
		x += int(this.entries[i].Header.DrawingBoxWdith)
	}

	//save to disk
	_, filename := path.Split(this.filename)
	os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)
	savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + "imageset"
	file, _ := os.Create(savePath + ".png")
	png.Encode(file, img)
	file.Close()
}

func (this *GM1Image) SaveTileset() {
	numberOfEntries := len(this.entries)
	tilesPerRow := int(math.Sqrt(float64(numberOfEntries)))

	var x, y int

	//create image
	img := image.NewRGBA(image.Rect(0, 0, tilesPerRow*GM1TILEWIDTH, tilesPerRow*GM1TILEHEIGHT))

	//draw tiles
	for i := 0; i < numberOfEntries; i++ {
		rect := image.Rect(x*GM1TILEWIDTH, y*GM1TILEHEIGHT, (x+1)*GM1TILEWIDTH, (y+1)*GM1TILEHEIGHT)
		draw.Draw(img, rect, this.entries[i].Tile.data, image.ZP, draw.Over)
		x++
		if x > tilesPerRow {
			x = 0
			y++
		}
	}

	//save to disk
	_, filename := path.Split(this.filename)
	os.MkdirAll("output/"+PathTypeTable[int(this.header.DataType)]+"/"+filename, os.ModeDir|os.ModePerm)
	savePath := "output/" + PathTypeTable[int(this.header.DataType)] + "/" + filename + "/" + "tileset"
	file, _ := os.Create(savePath + ".png")
	png.Encode(file, img)
	file.Close()
}

func (this *GM1Image) SmartSave() {
	if this.header.DataType == TYPE_TGXTILE {
		if build {
			this.SaveCollection()
			this.SaveImageset()
		}
		if buildTileset {
			this.SaveTileset()
		}
	} else {
		this.Save()
	}
}
